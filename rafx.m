%polecenie 1 h/(2?)
 h = 6.62607015E-34 %establishing plancks constant
 h/2*pi     %calculating the value
 
 %polecenie 2 sin(30°/e)
 e=exp(1) %constant e
 sind(30/e) %sind works on degrees while sin works on radians 
 
 %polecenie 3 0x00123d3 / (2,455·1023)
 dec=hex2dec('00123d3') %function hex2dec turns hexadecimal numbers into decimal
 dec/2,455E23 %calculating the value
 
 %polecenie 4 ?e??
 e=exp(1) %constant e
 sqrt(e-pi) %function sqrt calculates square root
 
 %polecenie 5 Wyświetlić 10. miejsce po przecinku liczby ?
 
 
 %polecenie 6 Ile dni upłynęło o daty Twoich urodzin? (works in scilab)
 //exercise 6
t1=[1999 08 18 11 00 13.535] //my birthday
t2=clock()  //actual date
E1=etime(t2(),t1)/(3600 * 24)
 
 
 %polecenie 7 arctg
 liczba=hex2dec('aabb') %turning hexadecimal number into decimal
 R=6371 %establishing Earths radius in km
 atan((exp((sqrt(7)/2)-log(R/10E5)))/liczba) %calculating the value
 
 %polecenie 8  Obliczyć liczbę atomów w 1/5 mikromola alkoholu etylowego
 avogadr=6.02214076E23 %establishing avogadros constant
 atoms=9 %number of atoms in ethanol 
 
 (1/5)*atoms*10E-6*avogadr %calculating the value (number of atoms in 1/5 micro mol of ethanol)
 
 